//
//  ViewController.swift
//  W08_Jarrell_Cliff
//
//  Created by Clifford Kyle Jarrell on 10/12/18.
//  Copyright © 2018 OSU CS Department. All rights reserved.
//

// Hide keyboard on return button click and tapping on screen

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfDepartment: UITextField!
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    
    @IBAction func gstrTap(_ sender: Any) {
        hideKeyboard()
    }
    
    var deptAbbrResult = ""
    var courseNumResult: Int16 = 0
    var courseTitleResult = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // delegate the text fields
        self.tfDepartment.delegate = self
        self.tfNumber.delegate = self
        self.tfTitle.delegate = self
    }

    // define the return function on keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        hideKeyboard()
        
        return true
    }
    
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        deptAbbrResult = tfDepartment.text ?? "Bad Department Abbreviation"
        courseNumResult = Int16(tfNumber.text ?? "-1")!
        courseTitleResult = tfTitle.text ?? "Bad Title"
    }
    
    // detect if there is data in the text fields
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if (tfDepartment.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0
            && tfNumber.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0
            && tfTitle.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0) {
            return true
        }
        else {
            showAlert("Enter a value for all fields or press \"Back\" to cancel")
            return false
        }
    }
    
    
    // generates simple alert dialog with a given message
    func showAlert(_ message: String) {
        let okAction = UIAlertAction(title: "OK!", style: .default, handler: nil)
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(okAction)  // Present the alert controller to the user.
        present(alertController, animated: true,     completion: nil)
    }
}


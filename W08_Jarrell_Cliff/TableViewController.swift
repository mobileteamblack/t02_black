//
//  TableViewController.swift
//  W08_Jarrell_Cliff
//
//  Created by Clifford Kyle Jarrell on 10/12/18.
//  Copyright © 2018 OSU CS Department. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {

    var dataSource: [NSManagedObject] = []
    
    var appDelegate: AppDelegate?
    var context: NSManagedObjectContext?
    var entity: NSEntityDescription?
    
    var duplicateEntered = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate?.persistentContainer.viewContext
        entity = NSEntityDescription.entity(forEntityName: "Course", in: context!)
    }
    
    // Save the data from the form to the database
    // called before viewWillAppear
    @IBAction func unwindFromSave(segue: UIStoryboardSegue) {
        
        // reset
        duplicateEntered = false
    
        // Get the segue source
        guard let source = segue.source as? ViewController else {
            print("Cannot get unwind segue source")
            return
        }
        
        if duplicate(source: source) {
            duplicateEntered = true
            return
        }
        else {
            if let entity = self.entity {
                // Create new course record
                let course = NSManagedObject(entity: entity, insertInto: context)
                // Set the record attributes
                course.setValue(source.deptAbbrResult, forKey: "deptAbbr")
                course.setValue(source.courseTitleResult, forKey: "title")
                course.setValue(source.courseNumResult, forKey: "courseNum")
                
                do {
                    // Update the data store
                    try context?.save()
                    // Add to data source for table view
                    dataSource.append(course)
                    self.tableView.reloadData()
                }
                catch let error as NSError {
                    print("Cannot save data: \(error)")
                }
            }
        }
    }
    
    // check to see if data entered is a duplicate
    func duplicate(source: ViewController) -> Bool {
        for instance in dataSource {
            if instance.value(forKey: "courseNum") as! Int16 == source.courseNumResult {
                if instance.value(forKey: "deptAbbr") as! String == source.deptAbbrResult {
                    return true
                }
            }
        }
        return false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "My Cell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = dataSource[indexPath[1]].value(forKey: "deptAbbr")
            as! String +
            String((dataSource[indexPath[1]]).value(forKey: "courseNum") as! Int16)
        
        cell.detailTextLabel?.text = dataSource[indexPath[1]].value(forKey: "title") as? String
        
        return cell
    }
    
    // before view appears, let dataSource be the newly updated Course entity
    override func viewWillAppear(_ animated: Bool) {
        // Fetch database contents
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Course")
        
        
        do {
            dataSource = try context?.fetch(fetchRequest) ?? []
        }
        catch let error as NSError {
            print("Cannot load data: \(error)")
        }
        
        
        if duplicateEntered == true {
            showAlert("Duplicate entered and will not be saved")
        }
    }
    
    // generates simple alert dialog with a given message
    func showAlert(_ message: String) {
        let okAction = UIAlertAction(title: "OK!", style: .default, handler: nil)
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(okAction)  // Present the alert controller to the user.
        present(alertController, animated: true,     completion: nil)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
